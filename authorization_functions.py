#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

import json
import re
import os

#  this file contents only authorization functions, which will use in JobTracker.py


def authorization_new(user_name, user_password):

    name = user_name
    password = user_password
    current = "N"

    #  load data from the user_file
    old_data_from_the_file, file_name, output_error_code = authorization_load()

    #  merge of new and old json-information
    old_data_from_the_file["User"].append({"name": name, "password": password, "current": current})

    #  write it
    try:
        with open(file_name, 'w') as file_for_tasks:
          json.dump(old_data_from_the_file, file_for_tasks, indent=2, ensure_ascii=False)
    except:
        output_error_code = 9

    return output_error_code


def authorization_change(user_name):

    #  information about of user, which will be current
    name = user_name
    password = ""

    #  load data from the user_file
    old_data_from_the_file, file_name, output_error_code = authorization_load()
    number_of_users_in_file = len(list(old_data_from_the_file["User"]))

    #  new data, which will be written into the file
    new_data_for_the_file = old_data_from_the_file

    if output_error_code == 0:
        #  search into the user_file for user_name
        number_of_editing_task = 1000
        for i in range(0, number_of_users_in_file):

            if old_data_from_the_file["User"][i]["name"] == name:
                number_of_editing_task = i
                password = old_data_from_the_file["User"][number_of_editing_task]["password"]
            else:
                #  if the user is not the searching one, than write information about others users into special form
                new_data_for_the_file["User"].append({"name": old_data_from_the_file["User"][i]["name"], "password": old_data_from_the_file["User"][i]["password"], "current": "N"})
                del new_data_for_the_file["User"][i]

        if number_of_editing_task == 1000:
            output_error_code = 1
        else:
            #  if such user_name was found, than change parameter "current" to "Y"
            del new_data_for_the_file["User"][number_of_editing_task]
            new_data_for_the_file["User"].append({"name": name, "password": password, "current": "Y"})

            output_error_code = authorization_write(new_data_for_the_file)

    return output_error_code


def authorization_delete(user_name):

    output_error_code = 0

    #  load data from the user_file
    old_data_from_the_file, file_name, output_error_code = authorization_load()

    if output_error_code == 0:
        #  search into the user_file for user_name
        number_of_editing_task = 1000
        for i in range(len(list(old_data_from_the_file["User"]))):
            if old_data_from_the_file["User"][i]["name"] == user_name:
                number_of_editing_task = i
        if number_of_editing_task == 1000:
            output_error_code = 1
        else:
            #  if such user_name was found, than delete it
            del old_data_from_the_file["User"][number_of_editing_task]

            output_error_code = authorization_write(old_data_from_the_file)

    return output_error_code


def authorization_load():

    output_error_code = 0
    line = ['', '', '', '']

    #  load information about disposition of file, which contents user-info
    try:
        f = open('Config.md')
        line = f.readlines()
        line[0] = re.sub('\n', '', line[0])
        f.close()
    except:
        output_error_code = 2

    #  load user-info
    search_string = dict()
    try:
        with open(os.path.expanduser(line[0]), 'r') as data_from_the_file:
            search_string = json.load(data_from_the_file)
    except:
        output_error_code = 9

    return search_string, line[0], output_error_code


def authorization_write(new_data):
    output_error_code = 0
    line = ['', '', '']

    #  load information about disposition of file, which contents user-info
    try:
        f = open('Config.md')
        line = f.readlines()
        line[0] = re.sub('\n', '', line[0])
        f.close()
    except:
        output_error_code = 2

    #  load user-info
    search_string = new_data
    try:
        with open(line[0], 'w') as file_for_tasks:
            json.dump(search_string, file_for_tasks, indent=2, ensure_ascii=False)
    except:
        output_error_code = 9

    return output_error_code