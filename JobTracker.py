#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

import os
import re
import argparse
import json
from datetime import datetime
import authorization_functions
import logging


# import unit_tests


def configuration_loading(config_type):
    #  configuration-info about of location of json- and log- files

    line = ['', '', '', '', '', '', '', '', '', '']
    output_error_code = 0
    output_disposition = ""

    try:
        f = open('Config.md')
        line = f.readlines()

        #  choosing file, which user want to use
        if config_type == "u":
            line[0] = re.sub('\n', '', line[0])
            output_disposition = line[0]
        if config_type == "t":
            line[1] = re.sub('\n', '', line[1])
            output_disposition = line[1]
        if config_type == "r":
            line[2] = re.sub('\n', '', line[2])
            output_disposition = line[2]
        if config_type == "l":
            line[3] = re.sub('\n', '', line[3])
            output_disposition = line[3]
        f.close()
    except:
        output_error_code = 2

    return output_disposition, output_error_code


class SimpleTask(object):
    def __init__(self, task_name, task_condition, task_priority, task_date_of_execution, task_status, task_executor):
        self.name = task_name  # name of task
        self.condition = task_condition  # goal of the task
        self.priority = task_priority  # priority of the task
        self.date_of_execution = task_date_of_execution  # date, when task must be ended
        self.status = task_status  # implementation progress
        self.executor = task_executor  # name of user, who will do this task

    def writing_to_the_file_new_simple_task(self, task_name, task_condition, task_priority, task_date_of_execution,
                                            task_status, task_executor):
        #  preparing to write task name

        #  load config-info
        disposition_of_task_file_ws, er_ws = configuration_loading("t")

        #  load old tasks
        try:
            data_from_the_files = json.load(open(disposition_of_task_file_ws))
        except:
            data_from_the_files = dict()

        #  merge of new and old json-information
        data_from_the_files["SimpleTask"].append({"name": task_name, "condition": task_condition,
                                                  "priority": task_priority,
                                                  "date_of_execution": task_date_of_execution,
                                                  "status": task_status, "executor": task_executor})

        with open(disposition_of_task_file_ws, 'w') as file_for_tasks:
            json.dump(data_from_the_files, file_for_tasks, indent=2, ensure_ascii=False)


class ComboTask(object):
    def __init__(self, ct_name, ct_goal, ct_type_of_execution, ct_executor):
        self.name = ct_name  # name of task
        self.goal = ct_goal  # goal of the task
        self.type_of_execution = ct_type_of_execution  # type of using this task
        self.executor = ct_executor  # name of user, who will do this task

    def writing_to_the_file_new_combo_task(self, ct_name, ct_goal, ct_type_of_execution, ct_executor):

        #  preparing to write task

        #  load config-info
        disposition_of_task_file_wc, er_wc = configuration_loading("t")

        #  load old tasks
        try:
            data_from_the_files = json.load(open(disposition_of_task_file_wc))
        except:
            data_from_the_files = dict()

        sub_name = ""
        sub_condition = ""
        sub_priority = ""
        sub_date_of_execution = ""
        sub_status = ""
        sub_executor = ""
        sub_list = []

        #  merge of new and old json-information
        data_from_the_files["ComboTask"].append({"name": ct_name, "goal": ct_goal,
                                                 "type_of_execution": ct_type_of_execution,
                                                 "executor": ct_executor, "subtasks": sub_list})

        i = len(list(data_from_the_files["ComboTask"]))
        data_from_the_files["ComboTask"][i - 1]["subtasks"].append({"name": sub_name,
                                                                    "condition": sub_condition,
                                                                    "priority": sub_priority,
                                                                    "date_of_execution": sub_date_of_execution,
                                                                    "status": sub_status, "executor": sub_executor})

        with open(disposition_of_task_file_wc, 'w') as file_for_tasks:
            json.dump(data_from_the_files, file_for_tasks, indent=2, ensure_ascii=False)


class Reminder(object):
    def __init__(self, r_name, r_condition, r_frequency, r_start_date, r_finish_date, r_executor):
        self.name = r_name  # name of the reminder
        self.condition = r_condition  # condition of the reminder
        self.frequency = r_frequency  # frequency of occurrences the reminder
        self.start_date = r_start_date  # when the reminder was created
        self.finish_date = r_finish_date  # when the reminder will be disabled
        self.executor = r_executor  # name of user, for whom this reminder will create

    def writing_to_the_file_new_reminder(self, r_name, r_condition, r_frequency, r_start_date,
                                         r_finish_date, r_executor):
        #  preparing to write changes

        #  load config-info
        disposition_of_reminder_file_wr, er_wr = configuration_loading("r")

        #  load old reminders
        try:
            data_from_the_files = json.load(open(disposition_of_reminder_file_wr))
        except:
            data_from_the_files = dict()

        start_date_1 = str(r_start_date)
        finish_date_1 = str(r_finish_date)

        #  merge of new and old json-information
        data_from_the_files["Reminder"].append({"name": r_name, "condition": r_condition, "frequency": r_frequency,
                                                "date_of_execution": start_date_1, "finish_date": finish_date_1,
                                                "executor": r_executor})

        with open(disposition_of_reminder_file_wr, 'w') as file_for_tasks:
            json.dump(data_from_the_files, file_for_tasks, indent=2, ensure_ascii=False)


def finding_of_the_task(parameter, input_of_the_user):
    #  search the Task, which user want to find

    #  output data
    output_name = ""
    output_condition = ""
    output_goal = ""
    output_priority = ""
    output_date_of_execution = ""
    output_type_of_execution = ""
    output_status = ""
    output_executor = ""
    output_error_code = 0

    #  load config-info
    disposition_of_task_file_finding, er_f = configuration_loading("t")
    #  load all tasks
    search_string = dict()
    try:
        with open(os.path.expanduser(disposition_of_task_file_finding), 'r') as data_from_the_files:
            search_string = json.load(data_from_the_files)
    except:
        output_error_code = 9

    #  processing search_string
    if parameter == "n":
        output_error_code = 1
        for j in range(len(list(search_string["SimpleTask"]))):
            if search_string["SimpleTask"][j]["name"] == input_of_the_user:
                output_name = search_string["SimpleTask"][j]["name"]
                output_condition = search_string["SimpleTask"][j]["condition"]
                output_priority = str(int(search_string["SimpleTask"][j]["priority"]))
                output_date_of_execution = search_string["SimpleTask"][j]["date_of_execution"]
                output_status = search_string["SimpleTask"][j]["status"]
                output_executor = search_string["SimpleTask"][j]["executor"]
                output_error_code = 0
        for j in range(len(list(search_string["ComboTask"]))):
            if search_string["ComboTask"][j]["name"] == input_of_the_user:
                output_name = search_string["ComboTask"][j]["name"]
                output_goal = search_string["ComboTask"][j]["goal"]
                output_type_of_execution = search_string["ComboTask"][j]["type_of_execution"]
                output_executor = search_string["ComboTask"][j]["executor"]
                output_error_code = 0
    if parameter == "c":
        output_error_code = 2
        for j in range(len(list(search_string["SimpleTask"]))):
            if search_string["SimpleTask"][j]["condition"] == input_of_the_user:
                output_name = search_string["SimpleTask"][j]["name"]
                output_condition = search_string["SimpleTask"][j]["condition"]
                output_priority = str(int(search_string["SimpleTask"][j]["priority"]))
                output_date_of_execution = search_string["SimpleTask"][j]["date_of_execution"]
                output_status = search_string["SimpleTask"][j]["status"]
                output_executor = search_string["SimpleTask"][j]["executor"]
                output_error_code = 0
    if parameter == "g":
        output_error_code = 2
        for j in range(len(list(search_string["ComboTask"]))):
            if search_string["ComboTask"][j]["goal"] == input_of_the_user:
                output_name = search_string["ComboTask"][j]["name"]
                output_goal = search_string["ComboTask"][j]["goal"]
                output_type_of_execution = search_string["ComboTask"][j]["type_of_execution"]
                output_executor = search_string["ComboTask"][j]["executor"]
                output_error_code = 0
    if parameter == "p":
        output_error_code = 3
        for j in range(len(list(search_string["SimpleTask"]))):
            if search_string["SimpleTask"][j]["priority"] == int(input_of_the_user):
                output_name = search_string["SimpleTask"][j]["name"]
                output_condition = search_string["SimpleTask"][j]["condition"]
                output_priority = str(int(search_string["SimpleTask"][j]["priority"]))
                output_date_of_execution = search_string["SimpleTask"][j]["date_of_execution"]
                output_status = search_string["SimpleTask"][j]["status"]
                output_executor = search_string["SimpleTask"][j]["executor"]
                output_error_code = 0
    if parameter == "d":
        output_error_code = 4
        for j in range(len(list(search_string["SimpleTask"]))):
            if search_string["SimpleTask"][j]["date_of_execution"] == input_of_the_user:
                output_name = search_string["SimpleTask"][j]["name"]
                output_condition = search_string["SimpleTask"][j]["condition"]
                output_priority = str(int(search_string["SimpleTask"][j]["priority"]))
                output_date_of_execution = search_string["SimpleTask"][j]["date_of_execution"]
                output_status = search_string["SimpleTask"][j]["status"]
                output_executor = search_string["SimpleTask"][j]["executor"]
                output_error_code = 0

    return output_name, output_condition, output_goal, output_priority, output_date_of_execution, \
        output_type_of_execution, output_status, output_executor, output_error_code


def deleting_of_the_task(input_of_the_user):
    #  try to delete user's Task

    #  load config-info
    disposition_of_task_file_delete, er_d = configuration_loading("t")
    #  load all tasks
    search_string = dict()
    try:
        with open(os.path.expanduser(disposition_of_task_file_delete), 'r') as data_from_the_files:
            search_string = json.load(data_from_the_files)
    except:
        output_error_code = 9

    output_error_code = 1
    for i in range(len(list(search_string["SimpleTask"]))):
        if search_string["SimpleTask"][i]["name"] == input_of_the_user:
            number_of_deleting_task = i
            output_error_code = 0

            choice, output_error_code = choice_of_user("delete")

            #  deleting
            if choice == "y":
                try:
                    del search_string["SimpleTask"][number_of_deleting_task]

                    with open(disposition_of_task_file, 'w') as file_for_tasks:
                        json.dump(search_string, file_for_tasks, indent=2, ensure_ascii=False)

                except:
                    output_error_code = 9

            #  undo
            elif choice == "n":
                output_error_code = 3

    return output_error_code


def choice_of_user(type_of_def):
    #  det for find out the choice of user

    choicer_of_user = ""
    choicer_error_code = 0

    #  inputting choice of the user
    if type_of_def == "delete":
        print("Are you really sure, that you want to delete this task?")
        try:
            print("Write 'y', if you want to delete it, or 'n', if you want to cancel deleting")
            choicer_of_user = input("")
        except:
            choicer_error_code = 2

    return choicer_of_user, choicer_error_code


def displaying_old_information(type_of_def, info):
    info_for_edit_def = info

    if type_of_def == "n":
        print("Old name of task: " + info_for_edit_def)
        print("Pls, write new name")
    if type_of_def == "c":
        print("Old condition of task: " + info_for_edit_def)
        print("Pls, write new condition")
    if type_of_def == "p":
        print("Old priority of task: " + info_for_edit_def)
        print("Pls, write new priority")
    if type_of_def == "d":
        print("Old date of execution of task: " + info_for_edit_def)
        print("Pls, write new date of execution")
    if type_of_def == "s":
        print("Old status of task: " + info_for_edit_def)
        print("Pls, write new status")
    if type_of_def == "e":
        print("Old executor of task: " + info_for_edit_def)
        print("Pls, write new executor")

    return 0


def editing_of_the_task(input_of_the_user, edit_type):
    #  try to edit user's Task

    output_error_code = 10

    #  load config-info
    disposition_of_task_file_edit, er_e = configuration_loading("t")
    #  load all tasks
    search_string = dict()
    try:
        with open(os.path.expanduser(disposition_of_task_file_edit), 'r') as data_from_the_files:
            search_string = json.load(data_from_the_files)
    except:
        output_error_code = 9

    number_of_editing_task = 0
    for j in range(len(list(search_string["SimpleTask"]))):
        if search_string["SimpleTask"][j]["name"] == input_of_the_user:
            number_of_editing_task = j

    name_for_edit = search_string["SimpleTask"][number_of_editing_task]["name"]
    condition_for_edit = search_string["SimpleTask"][number_of_editing_task]["condition"]
    priority_for_edit = search_string["SimpleTask"][number_of_editing_task]["priority"]
    date_of_execution_for_edit = search_string["SimpleTask"][number_of_editing_task]["date_of_execution"]
    status_for_edit = search_string["SimpleTask"][number_of_editing_task]["status"]
    executor_for_edit = search_string["SimpleTask"][number_of_editing_task]["executor"]

    new_name = ""
    new_condition = ""
    new_priority = ""
    new_date_of_execution = ""
    new_status = ""
    new_executor = ""

    #  if parameter "Name" cheesed
    choice = edit_type
    if choice == "n":
        try:
            displaying_old_information(choice, name_for_edit)
            new_name = input("")
            output_error_code = 0
        except:
            output_error_code = 2
        name_for_edit = new_name

    #  if parameter "Condition" cheesed
    if choice == "c":
        try:
            displaying_old_information(choice, condition_for_edit)
            new_condition = input("")
            output_error_code = 0
        except:
            output_error_code = 2
        condition_for_edit = new_condition

    #  if parameter "Priority" cheesed
    if choice == "p":
        try:
            displaying_old_information(choice, priority_for_edit)
            new_priority = input("")
            output_error_code = 0
        except:
            output_error_code = 2
        priority_for_edit = new_priority

    #  if parameter "Date_of_execution_for_edit" cheesed
    if choice == "d":
        try:
            displaying_old_information(choice, date_of_execution_for_edit)
            new_date_of_execution = input("")
            output_error_code = 0
        except:
            output_error_code = 2
        date_of_execution_for_edit = new_date_of_execution

    #  if parameter "Status" cheesed
    if choice == "s":
        try:
            displaying_old_information(choice, status_for_edit)
            new_status = input("")
            output_error_code = 0
        except:
            output_error_code = 2
        status_for_edit = new_status

    #  if parameter "Executor" cheesed
    if choice == "e":
        try:
            displaying_old_information(choice, executor_for_edit)
            new_executor = input("")
            output_error_code = 0
        except:
            output_error_code = 2
        executor_for_edit = new_executor

    del search_string["SimpleTask"][number_of_editing_task]

    #  add update task to the json
    search_string["SimpleTask"].append({"name": name_for_edit, "condition": condition_for_edit,
                                        "priority": priority_for_edit, "date_of_execution": date_of_execution_for_edit,
                                        "status": status_for_edit, "executor": executor_for_edit})

    with open(disposition_of_task_file, 'w') as file_for_tasks:
        json.dump(search_string, file_for_tasks, indent=2, ensure_ascii=False)

    return output_error_code


def checking_of_notifications():
    #  try to show user information about tasks, which are "In_progress" or reminders, for this day

    #  load config-info
    disposition_of_task_file_check, er1 = configuration_loading("t")
    disposition_of_reminder_file_check, er2 = configuration_loading("t")
    tasks_string = dict()
    try:
        with open(os.path.expanduser(disposition_of_task_file_check), 'r') as data_from_the_tasks_file:
            tasks_string = json.load(data_from_the_tasks_file)
    except:
        print("File 'Tasks.json' does not exist")

    #  load all reminders
    reminders_string = dict()
    try:
        with open(os.path.expanduser(disposition_of_reminder_file_check), 'r') as data_from_the_reminders_file:
            reminders_string = json.load(data_from_the_reminders_file)
    except:
        print("File 'Reminders.json' does not exist")

    #  processing data about tasks
    print("Tasks for this day:")

    #  search data about this day
    right_now = datetime.today()
    now_date = str(right_now.date())

    #  reviewing all tasks, compare their date_of_execution with now_date
    for j in range(len(list(tasks_string["SimpleTask"]))):
        processing_date_of_the_task = str(tasks_string["SimpleTask"][j]["date_of_execution"])
        if processing_date_of_the_task == now_date:
            if tasks_string["SimpleTask"][j]["name"]:
                print(tasks_string["SimpleTask"][j]["name"])

    #  processing data about reminders
    print("Reminders for this day:")
    for k in range(len(list(reminders_string["Reminder"]))):

        #  reviewing all tasks, compare their date_of_execution with now_date and finish_date
        processing_date_of_the_reminder = str(reminders_string["Reminder"][k]["date_of_execution"])
        finish_date_of_the_reminder = str(reminders_string["Reminder"][k]["finish_date"])

        #  but still need to take into account the frequency of reminders
        processing_frequency_of_the_reminder = reminders_string["Reminder"][k]["frequency"]
        if processing_frequency_of_the_reminder == "ED":
            checking_today_year = int(processing_date_of_the_reminder[0:4])
            checking_today_month = int(processing_date_of_the_reminder[5:7])
            checking_today_day = int(processing_date_of_the_reminder[8:])

            checking_finish_year = int(finish_date_of_the_reminder[0:4])
            checking_finish_month = int(finish_date_of_the_reminder[5:7])
            checking_finish_day = int(finish_date_of_the_reminder[8:])

            #  and compare it
            step_of_success = 0
            if checking_today_year > checking_finish_year:
                step_of_success = 1
            else:
                if checking_today_month > checking_finish_month:
                    step_of_success = 2
                else:
                    if (checking_today_day <= checking_finish_day) and (checking_today_month == checking_finish_month):
                        step_of_success = 3
                    if checking_today_day <= checking_finish_day:
                        step_of_success = 3

            if step_of_success == 3:
                if reminders_string["Reminder"][k]["name"]:
                    print(reminders_string["Reminder"][k]["name"])
                else:
                    print("Some reminders are not available : incorrect name '' ")

        if processing_frequency_of_the_reminder == "EW":
            print()

        if processing_frequency_of_the_reminder == "EM":
            print()

        if processing_frequency_of_the_reminder == "EY":
            print()


if __name__ == '__main__':

    #  parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--info', action='store_true')
    parser.add_argument('-n', '--new', action='store_true')
    parser.add_argument('-s', '--show', action='store_true')
    parser.add_argument('-f', '--find', action='store_true')
    parser.add_argument('-d', '--delete', action='store_true')
    parser.add_argument('-e', '--edit', action='store_true')
    parser.add_argument('-c', '--check', action='store_true')
    parser.add_argument('-t', '--tests', action='store_true')
    parser.add_argument('-a', '--authorization', action='store_true')
    args = parser.parse_args()

    #  parser arguments
    info_arg = False
    new_arg = False
    show_arg = False
    find_arg = False
    delete_arg = False
    edit_arg = False
    check_arg = False
    tests_arg = False
    authorization_arg = False

    #  load config-info
    disposition_of_task_file_log, er_log = configuration_loading("l")
    #  logger description
    logger = logging.getLogger("MainLogger")
    logger.setLevel(logging.INFO)
    #  create the logging file handler
    fh = logging.FileHandler(disposition_of_task_file_log)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    #  add handler to logger object
    logger.addHandler(fh)

    logger.info("Program started")

    if args.info:
        info_arg = True

        logger.info("Information provided")

        print("Hello, user, I'd like to show you, what can I do:")
        print("-i ~ get information about about the program's capabilities")
        print("-s ~ show all the tasks, which was created")
        print("-n ~ create new task")
        print("-f ~ find task")
        print("-d ~ delete task")
        print("-e ~ edit already created task")
        print("-a ~ check authorization of user")
        # print("-t ~ start Unit-tests")
        # print("-c ~ check reminders and tasks for user")

        logger.info("Program finished correct")

    if args.new:
        new_arg = True

        #  type_of_task chooser
        new_select = ""
        #  simple task
        print("Pls, write 's', if you want to create simple task")
        #  combo task
        print("Pls, write 'c', if you want to create combined task")
        #  group task
        # print("Pls, write 'g', if you want to create group task")
        #  reminder
        print("Pls, write 'r', if you want to create reminder")

        try:
            new_select = input("")
        except:
            print("Pls, try again")
            logger.error("Error, while inputting type of task")

        if new_select == "s":

            #  new SimpleTask parameters input
            task = SimpleTask('', '', 0, '', '', '')

            #  new Task parameters input method

            try:
                task.name = input("Enter Task name: '...' ")
                task.condition = input("Enter condition of the Task: '...' ")
                task.priority = input("Enter priority of the Task: (int) ")
                task.date_of_execution = input("Enter date of execution of the Task(YYYY-MM-DD): '...' ")
                task.status = "In_progress"
                task.executor = ""
                print("Nice, everything was right")
            except:
                print("Something was wrong, while you entered the data. Pls, try again")
                logger.error("Error, while inputting parameters of task")

            # write it
            task.writing_to_the_file_new_simple_task(task.name, task.condition, task.priority, task.date_of_execution,
                                                     task.status, task.executor)
            logger.info("Program finished correct")

        if new_select == "c":

            #  new ComboTask parameters input
            task = ComboTask('', '', '', '')

            #  new Task parameters input method

            try:
                task.name = input("Enter Task name: '...' ")
                task.goal = input("Enter common goal of the Task: '...' ")
                task.type_of_execution = input("Enter task execution in series('S') or in parallel('P'): '...' ")
                task.executor = ""
                print("Nice, everything was right")
            except:
                print("Something was wrong, while you entered the data. Pls, try again")
                logger.error("Error, while inputting parameters of task")

            #  write it
            task.writing_to_the_file_new_combo_task(task.name, task.goal, task.type_of_execution, task.executor)
            logger.info("Program finished correct")

        if new_select == "r":

            #  new reminder parameters input
            reminder = Reminder('', '', '', '', '', '')

            #  new Reminder input method

            try:
                reminder.name = input("Enter Reminder name '...' ")
                reminder.condition = input("Enter Reminder condition '...' ")
                reminder.frequency = input(
                    "Enter Reminder frequency 'ED'(every day), 'EW'(week), 'EM'(month), 'EY'(year) ")
                reminder.executor = ""

                #  'now' is a time at this moment
                now = datetime.today()
                reminder.start_date = now.date()

                date_input_string = input("Enter Reminder finish_date 'YYYY-MM-DD' ")

                #  now I split date into pieces
                year = date_input_string[0:4]
                month = date_input_string[5:7]
                day = date_input_string[8:12]

                #  and concatenate it
                date_processing_string = datetime(int(year), int(month), int(day))

                #  and lastly, I put it into Reminder-object
                reminder.finish_date = date_processing_string.date()
                print("Cool, everything was right")
            except:
                print("Something was wrong, while you entered the data. Pls, try again")
                logger.error("Error, while inputting parameters of reminder")

            #  write it
            reminder.writing_to_the_file_new_reminder(reminder.name, reminder.condition, reminder.frequency,
                                                      reminder.start_date, reminder.finish_date, reminder.executor)
            logger.info("Program finished correct")

    if args.show:

        #  show all Tasks
        show_arg = True

        #  getting location of json file
        disposition_of_file = ""
        error_code = 0
        disposition_of_file, error_code = configuration_loading("t")

        try:
            data_for_show = json.load(open(disposition_of_file))
            if error_code == 0:
                print(data_for_show)
                logger.info("Information showed")
        except:
            print("Something was wrong. Pls, try again")
            logger.error("Error, while showing tasks")

    if args.find:

        #  find some Task
        find_arg = True

        #  parameters for finding
        print("Pls, write parameter, which you want to use, while finding")
        print("Pls, write 'n', if you remember name of the task")
        print("Pls, write 'c', if you remember condition of the task")
        print("Pls, write 'g', if you remember goal of the task")
        print("Pls, write 'p', if you remember priority of the task")
        print("Pls, write 'd', if you remember date of execution of the task")

        finding_select = ""
        try:
            finding_select = input("")
        except:
            print("Pls, try again")

        #  all input data
        find_string = ""
        name = ""
        condition = ""
        goal = ""
        priority = ""
        date_of_execution = ""
        type_of_execution = ""
        status = ""
        executor = ""
        error_code = 10

        if finding_select == "n":

            #  start search by parameter "name"
            try:
                print("Pls, write name of the task")
                find_string = input("")
                name, condition, goal, priority, date_of_execution, type_of_execution, status, executor, error_code \
                    = finding_of_the_task(finding_select, find_string)
            except:
                print("Something was wrong. Pls, try again")
                logger.error("Error, while searching")

        if finding_select == "c":

            #  start search by parameter "condition"
            try:
                print("Pls, write condition of the task")
                find_string = input("")
                name, condition, goal, priority, date_of_execution, type_of_execution, status, executor, error_code \
                    = finding_of_the_task(finding_select, find_string)
            except:
                print("Something was wrong. Pls, try again")
                logger.error("Error, while searching")

        if finding_select == "g":

            #  start search by parameter "goal"
            try:
                print("Pls, write goal of the task")
                find_string = input("")
                name, condition, goal, priority, date_of_execution, type_of_execution, status, executor, error_code \
                    = finding_of_the_task(finding_select, find_string)
            except:
                print("Something was wrong. Pls, try again")
                logger.error("Error, while searching")

        if finding_select == "p":

            #  start search by parameter "priority"
            try:
                print("Pls, write priority of the task")
                find_string = input("")
                name, condition, goal, priority, date_of_execution, type_of_execution, status, executor, error_code \
                    = finding_of_the_task(finding_select, find_string)
            except:
                print("Something was wrong. Pls, try again")
                logger.error("Error, while searching")

        if finding_select == "d":

            #  start search by parameter "date_of_execution"
            try:
                print("Pls, write date of execution of the task('YYYY-MM-DD')")
                find_string = input("")
                name, condition, goal, priority, date_of_execution, type_of_execution, status, executor, error_code \
                    = finding_of_the_task(finding_select, find_string)
            except:
                print("Something was wrong. Pls, try again")
                logger.error("Error, while searching")

        #  display information about task, which was found
        if error_code == 0:
            print("")
            print("I find it:")
            if name != "":
                print("Name of the Task: " + name)
            if condition != "":
                print("Condition of the Task: " + condition)
            if goal != "":
                print("Goal of the Task: " + goal)
            if priority != "":
                print("Priority of the Task: " + priority)
            if date_of_execution != "":
                print("Date of execution of the Task: " + date_of_execution)
            if type_of_execution != "":
                if type_of_execution == "S":
                    print("Type of execution of the Task: " + "in series")
                if type_of_execution == "P":
                    print("Type of execution of the Task: " + "in parallel")
            if status != "":
                print("Progress of implementation of the Task: " + status)
            if executor != "":
                print("Executor of the Task: " + executor)
            logger.info("Program finished correct")

        #  display information by mistake, if any
        if error_code == 1:
            print("There is no such name of the task")
            logger.warning("There is no such name of the task")
        if error_code == 2:
            print("There is no such condition or goal of the task")
            logger.warning("There is no such condition of the task")
        if error_code == 3:
            print("There is no such priority of the task")
            logger.warning("There is no such priority of the task")
        if error_code == 4:
            print("There is no such date of execution of the task")
            logger.warning("There is no such date of execution of the task")
        if error_code == 9:
            print("There is no such file for the tasks")
            logger.error("Error, there is no such file for the tasks")

    if args.delete:

        #  delete Task
        delete_arg = True

        error_code = 10

        print("Firstly, you need to find the task, that you want to delete")
        print("Pls, write name of the task")

        delete_input = ""

        try:
            delete_input = input("")

            search_str = dict()
            #  load config-info
            disposition_of_task_file, er = configuration_loading("t")
            try:
                with open(os.path.expanduser(disposition_of_task_file), 'r') as data_from_the_file:
                    search_str = json.load(data_from_the_file)
            except:
                error_code = 9

            for i in range(len(list(search_str["SimpleTask"]))):
                if search_str["SimpleTask"][i]["name"] == delete_input:
                    #  print info about task, which user want to delete
                    print("")
                    print("I find task, which you want to delete:")
                    print("Name of Task: " + search_str["SimpleTask"][i]["name"])
                    print("Condition of Task: " + search_str["SimpleTask"][i]["condition"])
                    print("Priority of Task: " + str(int(search_str["SimpleTask"][i]["priority"])))
                    print("Date of execution of Task: " + search_str["SimpleTask"][i]["date_of_execution"])
                    print("Progress of implementation of the Task: " + search_str["SimpleTask"][i]["status"])
                    print("Executor of the Task: " + search_str["SimpleTask"][i]["executor"])
                    print("")

            error_code = deleting_of_the_task(delete_input)

        except:
            print("Something was wrong. Pls, try again")
            logger.error("Error, while deleting")

        if error_code == 0:
            print("The execution took place!")
            logger.info("Program finished correct")
        if error_code == 1:
            print("There is no such name of the task")
            logger.warning("There is no such name of the task")
        if error_code == 2:
            print("Invalid input, pls, do it again!")
            logger.error("Error, while inputting")
        if error_code == 3:
            print("It will live... for now")
            logger.info("Program finished correct")
        if error_code == 9:
            print("There is no such file for the tasks")
            logger.error("Error, there is no such file for the tasks")

    if args.edit:

        #  edit Task
        edit_arg = True

        print("Firstly, you need to find the task, that you want to edit")
        print("Pls, write name of the task")

        edit_input = ""
        find_code = 0
        error_code = 10

        try:
            search_str = dict()
            #  load config-info
            disposition_of_task_file, er = configuration_loading("t")
            try:
                with open(os.path.expanduser(disposition_of_task_file), 'r') as data_from_the_file:
                    search_str = json.load(data_from_the_file)
            except:
                error_code = 9

            edit_input = input("")
            for i in range(len(list(search_str["SimpleTask"]))):
                if search_str["SimpleTask"][i]["name"] == edit_input:
                    #  print info about task, which user want to edit
                    print("")
                    print("I find task, which you want to edit:")
                    print("Name of Task: " + search_str["SimpleTask"][i]["name"])
                    print("Condition of Task: " + search_str["SimpleTask"][i]["condition"])
                    print("Priority of Task: " + str(int(search_str["SimpleTask"][i]["priority"])))
                    print("Date of execution of Task: " + search_str["SimpleTask"][i]["date_of_execution"])
                    print("Progress of implementation of the Task: " + search_str["SimpleTask"][i]["status"])
                    print("Executor of the Task: " + search_str["SimpleTask"][i]["executor"])
                    print("")
                    find_code = 1

            if find_code == 1:
                #  choosing of parameter, which user want to edit
                print("Pls, choose parameter, which you want to change")
                print("Write 'n', if you want to change Name of the task")
                print("Write 'c', if you want to change Condition of the task")
                print("Write 'p', if you want to change Priority of the task")
                print("Write 'd', if you want to change Date of execution of the task")
                print("Write 's', if you want to change Status of the task")
                print("Write 'e', if you want to change Executor of the task")
                edit_type_input = input("")
                print("")

                error_code = editing_of_the_task(edit_input, edit_type_input)
            else:
                print("Wrong name of the task, pls try again")
        except:
            print("Something was wrong. Pls, try again")
            logger.error("Error, while edit")

        if error_code == 0:
            print("All done!")
            logger.info("Program finished correct")
        if error_code == 1:
            print("")
            logger.error("Error, while edit")
        if error_code == 2:
            print("Something was wrong, pls try again")
            logger.error("Error, while edit")
        if error_code == 9:
            print("There is no such file for the tasks")
            logger.error("Error, there is no such file for the tasks")

    if args.check:
        #  check Task
        check_arg = True

        #  call function for check reminders and tasks for user, which are "In_progress"
        checking_of_notifications()

    if args.authorization:

        #  authorization of user
        authorization_arg = True

        authorization_select = ""
        current_user = ""
        error_code = 10

        #  load users data
        users = dict()
        #  load config-info
        disposition_of_user_file, er = configuration_loading("u")
        try:
            with open(os.path.expanduser(disposition_of_user_file), 'r') as data_from_the_users_file:
                users = json.load(data_from_the_users_file)
        except:
            print("File 'Users.json' does not exist")
            logger.error("Error, there is no such file for the tasks")

        for i in range(len(list(users["User"]))):
            if users["User"][i]["current"] == 'Y':
                current_user = users["User"][i]["name"]
                print("Current user is: " + current_user)
                break

        print("Pls, write 'n', if you want to create new user")
        print("Pls, write 'c', if you want to change user")
        print("Pls, write 'd', if you want to delete user")
        print("Pls, write 'e', if you want to exit")

        try:
            authorization_select = input("")
        except:
            print("Pls, try again")
            logger.error("Error, while inputting")

        if authorization_select == "n":
            new_name_input = ""
            new_password_input = ""

            try:
                print("Pls, write name of the new user")
                new_name_input = input("")
                print("Pls, write password of the new user")
                new_password_input = input("")

                error_code = authorization_functions.authorization_new(new_name_input, new_password_input)
            except:
                print("Something was wrong. Pls, try again")
                logger.error("Error, while authorization")

        if authorization_select == "c":
            name_input = ""

            try:
                print("Pls, write name of the user, which you want to choose")
                name_input = input("")

                error_code = authorization_functions.authorization_change(name_input)
            except:
                print("Something was wrong. Pls, try again")
                logger.error("Error, while authorization")

        if authorization_select == "d":
            name_input = ""

            try:
                print("Pls, write name of the user, which you want to delete")
                name_input = input("")

                error_code = authorization_functions.authorization_delete(name_input)
            except:
                print("Something was wrong. Pls, try again")
                logger.error("Error, while authorization")

        if authorization_select == "e":
            print("Bye")

        if error_code == 0:
            print("All done!")
            logger.info("Program finished correct")
        if error_code == 1:
            print("There is no such name of user")
            logger.warning("There is no such name of user")
        if error_code == 2:
            print("Something was wrong with configuration file")
            logger.error("Error, something was wrong with configuration file")
        if error_code == 9:
            print("There is no such file for the tasks")
            logger.error("Error, there is no such file for the tasks")

    if args.tests:
        #  Unit-tests arg
        tests_arg = True

        #  unit_tests()
